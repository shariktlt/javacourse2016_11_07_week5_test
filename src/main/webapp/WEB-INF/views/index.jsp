<%--
  Created by IntelliJ IDEA.
  User: innopolis
  Date: 07.11.16
  Time: 9:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>
    <title>Index</title>
  </head>
  <body>
  <a href="/admin">ROLE_ADMIN_PAGE</a> <a href="/user">ROLE_USER_PAGE</a>
  <sec:authorize access="isAuthenticated()">
    <a href="<c:url value="/logout" />">Logout</a>
  </sec:authorize>
  </body>
</html>
