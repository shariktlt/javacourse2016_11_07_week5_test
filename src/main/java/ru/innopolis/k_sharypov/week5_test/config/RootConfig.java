package ru.innopolis.k_sharypov.week5_test.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by innopolis on 06.11.16.
 */
@Configuration
@ComponentScan("ru.innopolis.k_sharypov.week5_test")
public class RootConfig {
}
