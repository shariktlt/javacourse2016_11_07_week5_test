package ru.innopolis.k_sharypov.week5_test.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by innopolis on 06.11.16.
 */

/**
 * Init security context
 */
public class SecurityInit extends AbstractSecurityWebApplicationInitializer {

}
